package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
)

const bookmarkFileHeader = `<!DOCTYPE NETSCAPE-Bookmark-file-1>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
<TITLE>Bookmarks</TITLE>
<H1>Bookmarks</H1>`

func logIfFatal(err error) {
	if err != nil {
		log.Fatal("Unexpected error:", err)
	}
}

// BookmarkRoots is the top level structure in the JSON object containing all of the bookmarks
type BookmarkRoots struct {
	Checksum string                    `json:"checksum"`
	Roots    map[string]BookmarkObject `json:"roots"`
	Version  int                       `json:"version"`
}

// BookmarkObject contains a slice of bookmark objects (which can be a URL or a folder/slice of more objects)
// mixed types in a JSON list is "fun"
type BookmarkObject struct {
	Items        []BookmarkObject `json:"children,omitempty"`
	DateModified string           `json:"date_modified,omitempty" xml:"MODIFIED_DATE,attr,omitempty"`
	URL          string           `json:"url,omitempty" xml:"HREF,attr,omitempty"`
	DateAdded    string           `json:"date_added" xml:"ADD_DATE,attr"`
	ID           string           `json:"id" xml:"-"`
	Name         string           `json:"name" xml:",chardata"`
	Type         string           `json:"type" xml:"TYPE,attr"`
}

// displayURL outputs the URL in XML format as a DefinitionTerm, DT is an element from a
// definition list https://www.w3.org/TR/html4/struct/lists.html#h-10.3
// https://en.wikipedia.org/wiki/HTML_element#dl
func displayURL(b BookmarkObject) {
	fmt.Printf("<DT><A HREF=\"%s\" ADD_DATE=\"%s\">%s</A>\n", b.URL, b.DateAdded, b.Name)
}

func displayFolderMetaData(b BookmarkObject) {
	fmt.Printf("<DT><H3 ADD_DATE=\"%s\" LAST_MODIFIED=\"%s\"", b.DateAdded, b.DateModified)
	if b.Name == "Bookmarks bar" {
		fmt.Printf(" PERSONAL_TOOLBAR_FOLDER=\"true\"")
	}
	fmt.Printf(">%s</H3>\n", b.Name)
}

func indent(depth int) {
	for i := 0; i < depth*4; i++ {
		fmt.Printf(" ")
	}
}

func recursiveDisplay(b BookmarkObject, depth int) {
	if b.Type == "folder" {
		indent(depth)
		displayFolderMetaData(b)
		indent(depth)
		fmt.Printf("<DL><p>\n")
	}
	if b.URL != "" {
		indent(depth)
		displayURL(b)
	}
	for _, v := range b.Items {
		recursiveDisplay(v, depth+1)
	}
	if b.Type == "folder" {
		indent(depth)
		fmt.Printf("</DL><p>\n")
	}
}

func main() {
	data, err := ioutil.ReadFile("Bookmarks")
	logIfFatal(err)

	var roots BookmarkRoots
	err = json.Unmarshal(data, &roots)
	logIfFatal(err)

	fmt.Println(bookmarkFileHeader)

	bar := roots.Roots["bookmark_bar"]
	// DEBUG: fmt.Println(bar.Name, bar.Type, bar.ID, bar.DateAdded, bar.DateModified)
	// Nested Definition Lists of Definition Terms comes from the "HTML Bookmark" standard
	// https://support.google.com/chrome/answer/96816?hl=en
	// http://kb.mozillazine.org/Bookmarks.html
	// https://msdn.microsoft.com/en-us/library/aa753582(v=vs.85).aspx

	fmt.Printf("<DL><p>\n")
	recursiveDisplay(bar, 1)
	fmt.Printf("</DL><p>\n")
	// TODO: XML output
}
