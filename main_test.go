package main

// go test
// go test -v -run TestParsingNestedAndVaried
// go test -cover

import (
	"encoding/json"
	"strconv"
	"testing"
)

var emptyFolderData = []byte(`
{
   "checksum": "c2f635678cd7c6b7adc5fc6789a333a9",
   "roots": {
      "other": {
         "children": [  ],
         "date_added": "12963388334079039",
         "date_modified": "13073675893574358",
         "id": "2",
         "name": "Other bookmarks",
         "type": "folder"
      },
      "synced": {
         "children": [  ],
         "date_added": "12963388334079052",
         "date_modified": "0",
         "id": "3",
         "name": "Mobile bookmarks",
         "type": "folder"
      },
      "bookmark_bar": {
         "children": [ ],
         "date_added": "12963388334079025",
         "date_modified": "13091341837695981",
         "id": "1",
         "name": "Bookmarks bar",
         "type": "folder"
      }
   },
   "version": 1
}
`)

var dataNested = []byte(`
{
   "checksum": "c2f635678cd7c6b7adc5fc6789a333a9",
   "roots": {
      "bookmark_bar": {
         "children": [ {
            "children": [ ],
            "date_added": "12964393702541822",
            "date_modified": "13072676730744049",
            "id": "4",
            "name": "News",
            "type": "folder"
         } ],
         "date_added": "12963388334079025",
         "date_modified": "13091341837695981",
         "id": "1",
         "name": "Bookmarks bar",
         "type": "folder"
      }
   },
   "version": 1
}
`)

var dataNestedAndVaried = []byte(`
{
   "version": 1,
   "checksum": "c2f635678cd7c6b7adc5fc6789a333a9",
   "roots": {
      "bookmark_bar": {
         "children": [ {
            "children": [ {
               "children": [ ],
               "date_added": "13048359450689931",
               "date_modified": "13055279859759897",
               "id": "232",
               "name": "cloud",
               "type": "folder"
            }, {
               "date_added": "13048359564636627",
               "id": "239",
               "name": "Re/code",
               "type": "url",
               "url": "http://recode.net/"
            }, {
               "date_added": "12968104203789258",
               "id": "71",
               "name": "Hacker News",
               "type": "url",
               "url": "https://news.ycombinator.com/"
            } ],
            "date_added": "12964393702541822",
            "date_modified": "13072676730744049",
            "id": "4",
            "name": "News",
            "type": "folder"
         }, {
            "date_added": "13018334978867413",
            "id": "191",
            "name": "AppEngine",
            "type": "url",
            "url": "https://appengine.google.com/"
         } ],
         "date_added": "12963388334079025",
         "date_modified": "13091341837695981",
         "id": "1",
         "name": "Bookmarks bar",
         "type": "folder"
      }
   }
}
`)

var dataExampleURLJSON = []byte(`
{
   "version": 1,
   "checksum": "c2f635678cd7c6b7adc5fc6789a333a9",
   "roots": {
      "bookmark_bar": {
         "children": [ {
			"date_added": "13018334978867413",
            "id": "191",
            "name": "Canonical URL Example",
            "type": "url",
            "url": "https://example.com"
		 } ],
         "date_added": "12963388334079025",
         "date_modified": "13091341837695981",
         "id": "1",
         "name": "Bookmarks bar",
         "type": "folder"
      }
	}
}
`)

var expectedHTML = []byte(`
<!DOCTYPE NETSCAPE-Bookmark-file-1>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
<TITLE>Bookmarks</TITLE>
<H1>Bookmarks</H1>
<DL><p>
    <DT><H3 ADD_DATE="1449633644" LAST_MODIFIED="1449636120" PERSONAL_TOOLBAR_FOLDER="true">Bookmarks bar</H3>
	<DL><p>
		<DT><H3 ADD_DATE="1472439693" LAST_MODIFIED="1472439709">1 folder</H3>
		<DL><p>
			<DT><H3 ADD_DATE="1472439712" LAST_MODIFIED="1472439712">2 folder</H3>
			<DL><p>
				<DT><H3 ADD_DATE="1472439809" LAST_MODIFIED="1472439809">3 folder</H3>
				<DL><p>
					<DT><A HREF="https://example.com/3" ADD_DATE="1472439809">3</A>
				</DL><p>
				<DT><A HREF="https://example.com/2" ADD_DATE="1472439712">2</A>
			</DL><p>
			<DT><A HREF="https://example.com/1" ADD_DATE="1472439709">1</A>
		</DL><p>
    </DL><p>
</DL><p>
`)

var ExampleURL = BookmarkObject{Type: "url", DateAdded: "13018334978867413", URL: "https://example.com",
	Name: "Canonical URL Example"}

func assertEqual(t *testing.T, a string, b string) {
	if a != b {
		t.Error("Expected,", a, "but received", b)
	}
}

// TODO: convert to a test matrix
func TestBookmarkRoots(t *testing.T) {
	var roots BookmarkRoots
	err := json.Unmarshal(emptyFolderData, &roots)
	logIfFatal(err)
	assertEqual(t, "c2f635678cd7c6b7adc5fc6789a333a9", roots.Checksum)
	assertEqual(t, "1", strconv.Itoa(roots.Version))

	bar := roots.Roots["bookmark_bar"]
	assertEqual(t, "12963388334079025", bar.DateAdded)
	assertEqual(t, "13091341837695981", bar.DateModified)
	assertEqual(t, "1", bar.ID)
	assertEqual(t, "Bookmarks bar", bar.Name)
	assertEqual(t, "folder", bar.Type)
	// TODO: assert children as an empty slice

	other := roots.Roots["other"]
	assertEqual(t, "12963388334079039", other.DateAdded)
	assertEqual(t, "13073675893574358", other.DateModified)
	assertEqual(t, "2", other.ID)
	assertEqual(t, "Other bookmarks", other.Name)
	assertEqual(t, "folder", other.Type)
	// TODO: assert children as an empty slice

	synced := roots.Roots["synced"]
	assertEqual(t, "12963388334079052", synced.DateAdded)
	assertEqual(t, "0", synced.DateModified)
	assertEqual(t, "3", synced.ID)
	assertEqual(t, "Mobile bookmarks", synced.Name)
	assertEqual(t, "folder", synced.Type)
	// TODO: assert children as an empty slice
}
